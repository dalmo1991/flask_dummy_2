from app_project import application
from flask import render_template

@application.route('/')
def home():
    return render_template('home.html')

if __name__ == "__main__":
    application.run(debug=True, host='0.0.0.0')