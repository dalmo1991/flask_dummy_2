Flask==1.1.1
Flask-Migrate==2.5.3
Flask-SQLAlchemy==2.4.1
Flask-WTF==0.14.3
SQLAlchemy==1.3.15
WTForms==2.2.1