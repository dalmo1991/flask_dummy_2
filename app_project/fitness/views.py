from flask import Blueprint, render_template, redirect, url_for, session
from .forms import Create
from app_project import db
from app_project.models import Fitness

fitness_blueprint = Blueprint(name='fitness', import_name=__name__,
                              template_folder='templates/fitness')

# Add
@fitness_blueprint.route('/add', methods=['GET', 'POST'])
def add():

    form = Create()

    if form.validate_on_submit():
        weight = form.weight.data
        height = form.height.data
        id = session['id']

        user_record = Fitness.query.filter_by(person_id=id).all()

        if len(user_record) == 0:
            data = Fitness(weight=weight, height=height, person_id=id)
            db.session.add(data)
            db.session.commit()
        else:
            data = user_record[0] # Only one possible
            data.weight = weight
            data.height = height
            # Don't have to change id
            db.session.add(data)
            db.session.commit()

        return redirect(url_for('people.dashboard'))

    return render_template('add_fit.html', form=form)
