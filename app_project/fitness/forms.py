from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField

class Create(FlaskForm):

    weight = IntegerField(label='Weight (only integer): ')
    height = IntegerField(label='Height (only integer): ')
    submit = SubmitField(label='Create')