import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import yaml

basedir = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(basedir, 'accounts.yml')) as f:
    sensible_data = yaml.load(f, Loader=yaml.FullLoader)

application = Flask(__name__)

application.config['SECRET_KEY'] = 'mysecretkey'
# Local
application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
# Deployment
# user = sensible_data['aws_rds']['master_username']
# password = sensible_data['aws_rds']['password']
# endpoint = sensible_data['aws_rds']['endpoint']
# db_name = sensible_data['aws_rds']['db_name']
# application.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}:3306/{}'.format(user, password, endpoint, db_name)


application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app=application)

# Local
Migrate(app=application, db=db)

# This has to be done after the db
from app_project.people.views import people_blueprint
from app_project.fitness.views import fitness_blueprint

application.register_blueprint(people_blueprint,url_prefix="/people")
application.register_blueprint(fitness_blueprint,url_prefix='/fitness')
