# This file contains the tables of the database

from app_project import db

class Person(db.Model):

    __tablename__ = 'people'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    age = db.Column(db.Integer)

    fitness = db.relationship('Fitness', backref='Puppy', uselist=False)

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __repr__(self):

        out = 'ID: {}, Name: {}, Age: {}, '.format(self.id, self.name, self.age)

        if self.fitness:
            out += 'Weight: {}, Height: {}'.format(self.fitness.weight, self.fitness.height)
        else:
            out += 'Weight: {}, Height: {}'.format(None, None)

        return out

class Fitness(db.Model):

    __tablename__ = 'fitness'

    id = db.Column(db.Integer, primary_key=True)
    weight = db.Column(db.Integer)
    height = db.Column(db.Integer)
    person_id = db.Column(db.Integer, db.ForeignKey('people.id'))

    def __init__(self, weight, height, person_id):

        self.weight = weight
        self.height = height
        self.person_id = person_id


