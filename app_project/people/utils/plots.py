import plotly.express as px
import numpy as np
import pandas as pd
import json
from plotly.utils import PlotlyJSONEncoder

np.random.seed(seed=0)

data = pd.DataFrame(np.random.randn(100, 3)*100,
                    columns=['Age', 'Weight', 'Height'])

plot1 = px.scatter_3d(data_frame=data, x='Age', y='Weight', z='Height')
plot2 = px.density_contour(data_frame=data, x='Weight', y='Height')
plot3 = px.histogram(data_frame=data, x='Age')

plot1_json = json.dumps(plot1, cls=PlotlyJSONEncoder)
plot2_json = json.dumps(plot2, cls=PlotlyJSONEncoder)
plot3_json = json.dumps(plot3, cls=PlotlyJSONEncoder)