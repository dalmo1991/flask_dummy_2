from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField

class Create(FlaskForm):

    name = StringField(label='Name of the user: ')
    age = IntegerField(label='Age: ')
    submit = SubmitField(label='Create')

class Delete(FlaskForm):

    id = IntegerField(label='User id to delete: ')
    submit = SubmitField(label='Delete!')

class Modify(FlaskForm):

    id = IntegerField(label='User id to modify: ')
    submit = SubmitField(label='Change')
