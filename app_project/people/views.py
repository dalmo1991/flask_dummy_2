from flask import Blueprint, render_template, redirect, url_for, session
from .forms import Create, Delete, Modify
from app_project import db
from app_project.models import Person, Fitness
from .utils.plots import plot1_json, plot2_json, plot3_json

people_blueprint = Blueprint(name='people', import_name=__name__,
                             template_folder='templates/people')

# Add
@people_blueprint.route('/add', methods=['GET', 'POST'])
def add():

    form = Create()

    if form.validate_on_submit():
        name = form.name.data
        age = form.age.data
        new_person = Person(name=name, age=age)

        db.session.add(new_person)
        db.session.commit()

        session['id'] = new_person.id
        return redirect(url_for('people.dashboard'))

    return render_template('add.html', form=form)

# Dashboard
@people_blueprint.route('/dashboard')
def dashboard():

    return render_template('dashboard.html')

# Modify
@people_blueprint.route('/modify', methods=['GET', 'POST'])
def modify():

    form = Modify()

    if form.validate_on_submit():
        session['id'] = form.id.data
        return redirect(url_for('people.dashboard'))

    return render_template('modify.html', form=form)

# Delete
@people_blueprint.route('/delete', methods=['GET', 'POST'])
def delete():

    form = Delete()

    if form.validate_on_submit():
        id = form.id.data
        
        # Delete person
        person = Person.query.get(id)
        db.session.delete(person)
        db.session.commit()
        # Delete data in fitness
        fit_data = Fitness.query.filter_by(person_id=id).all()

        if len(fit_data) != 0:
            db.session.delete(fit_data[0])
            db.session.commit()
        
        return redirect(url_for('home'))

    return render_template('delete.html', form=form)

# Modify
@people_blueprint.route('/show')
def show():

    people = Person.query.all()
    session['id'] = None
    return render_template('show.html', people=people,
                           p1=plot1_json,
                           p2=plot2_json,
                           p3=plot3_json)
