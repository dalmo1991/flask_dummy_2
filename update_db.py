import os
import sys

message = sys.argv[1]

print('Message: {}'.format(message))

this_folder = os.path.abspath(os.path.dirname(__file__))

print('This folder is: {}'.format(this_folder))

os.system('cd {}'.format(this_folder))
os.system('export FLASK_APP=application.py')
# os.system('flask db init')
os.system('flask db migrate -m \'{}\''.format(message))
os.system('flask db upgrade')
